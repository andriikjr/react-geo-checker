import { Platform, PermissionsAndroid } from 'react-native';
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';
import appendToLog from './files';

export const startTracking = () => {
    const interval = 30000;
    BackgroundGeolocation.configure({
        desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
        distanceFilter : 0,
        notificationTitle: 'Удачной поездки',
        notificationText:
          'Мы начинаем отслеживать ваше местоположение, что бы ваш заказчик был спокоен за свой контейнер',
        debug: false,
        startOnBoot: false,
        stopOnTerminate: true,
        locationProvider:
          Platform.OS === 'ios'
            ? BackgroundGeolocation.DISTANCE_FILTER_PROVIDER
            : BackgroundGeolocation.ACTIVITY_PROVIDER,
        interval: interval,
        fastestInterval: interval,
        activitiesInterval: interval,
        stopOnStillActivity: false,
        url: undefined,
      });
    setHandlers();

    BackgroundGeolocation.start();
};

export const setHandlers = () => {
        BackgroundGeolocation.on('location', (location) => {
            const checkWritingPermission = async () => {
            const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
                title: 'Write Trickiy permission',
                message: 'message for Trockiy',
                buttonNeutral: 'Ask Me Later',
                buttonNegative: 'Cancel',
                buttonPositive: 'OK',
            });

            if (granted === PermissionsAndroid.RESULTS.GRANTED) appendToLog(JSON.stringify(location));
            else checkWritingPermission();
        }

        checkWritingPermission()
        console.log(location);
        });

        BackgroundGeolocation.on('start', () => {
            console.log('[INFO] BackgroundGeolocation service has been started');
          });
};