import RNFS from 'react-native-fs';

const filePath = `${RNFS.ExternalStorageDirectoryPath}/log.txt`;

const readContents = () =>
    RNFS.readDir(RNFS.ExternalStorageDirectoryPath)
  .then((result) => Promise.all([RNFS.stat(result[0].path), result[0].path]))
  .then((statResult) => {
    if (statResult[0].isFile()) {
      // if we have a file, read it
      return RNFS.readFile(statResult[1], 'utf8');
    }

    return '';
  })
  .catch((err) => {
    console.log(err.message, err.code);
  });

const appendToLog = async (contents) => {
    let oldContents = '';
    try {
        oldContents = await  RNFS.readFile(filePath, 'utf8');
    } catch (error) {}
    RNFS.writeFile(filePath, `${oldContents}\n${contents}`, 'utf8').then((success) => console.log('file written')).catch((error) => console.log(error));
};

export default appendToLog;