import React, {useEffect} from 'react';
import { View, PermissionsAndroid } from 'react-native';
import { startTracking } from '../utils/tracker';

const App = (props) => {
  useEffect(() => {
		const runGeoLib = async () => {
			const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
				title: 'Geolocation ermission',
				message: 'message for Trockiy',
				buttonNeutral: 'Ask Me Later',
				buttonNegative: 'Cancel',
				buttonPositive: 'OK',
			});

			if (granted === PermissionsAndroid.RESULTS.GRANTED) startTracking();
			else runGeoLib();
		};

		runGeoLib();
	}, []);

  return (<View></View>)
};

export default App;